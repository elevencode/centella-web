<?php

if (!defined('ABSPATH')) exit();

/* MySQL database table prefix. */
$table_prefix = 'wp_';

/* Authentication Unique Keys and Salts. */
/* https://api.wordpress.org/secret-key/1.1/salt/ */
define('AUTH_KEY',         ';;)jA[m*UJ]C8{{[vHpH)zOGBDV8&T01VZ=I&:~aVrrF=$||B<1a(#(+j#m~n[TU');
define('SECURE_AUTH_KEY',  'b{m4d#o8n7<N}P5`],>aGpa{wboIv/d/E|6h=eu!08bzq:|5:vfJMLD]DL-,eZph');
define('LOGGED_IN_KEY',    'ibF-#S5`0%*9F+]sgNs}{hoRE!dM|D+YyGN|xkR:[4v|<gH3.o.o }(l+ebp9q=f');
define('NONCE_KEY',        'oXxeE&Fj<S[P<Ul?||$y9|)fQt@42s|}ze@).Psw(u7OmNP+${.RD.ckH/]r<vE>');
define('AUTH_SALT',        '7)]+*O;z7XNpGqE4a1,)cl26;F~<NJA=GR^aRYZ5Og!D`V1&XtiN0Lvi 6RozOVr');
define('SECURE_AUTH_SALT', '3c77rOO|@jM^|MNLO]OyOo{c!yYBj^LNuBzFiwrD8*2@LY}1&)LrYo#(]*=>5u1P');
define('LOGGED_IN_SALT',   'ZLK-qryn<pN }+(+R}Q5k@v+r%GG-gz[K9`CeoV~/S> K1n`JVN$|WantnME0hHA');
define('NONCE_SALT',       '}Yxv}a/K<hN;[!z%GNxS87kcPrDb7$7)Jivk/Z,+Q{8*BJQbr{*0f,.UM*q>p4y ');

/* BASIC SETTINGS */

/* for Contact-Form-7 */
define('WPCF7_AUTOP', false);

/* Specify maximum number of Revisions. */
define('WP_POST_REVISIONS', '3');

/* Media Trash. */
define('MEDIA_TRASH', false);

/* Updates */
define('WP_AUTO_UPDATE_CORE', true);
define('DISALLOW_FILE_MODS', true);
define('DISALLOW_FILE_EDIT', true);

/* Autosave interval */
define('AUTOSAVE_INTERVAL', 300); // Seconds

/* Debug mode */
//@ini_set('log_errors', 'On');
//@ini_set('display_errors', 'Off');
//define('WP_DEBUG', true);
//define('WP_DEBUG_LOG', true);
//define('WP_DEBUG_DISPLAY', true);
//define('ALLOW_UNFILTERED_UPLOADS', true);

/* Multisite. */
// define( 'WP_ALLOW_MULTISITE', false );

/* Language Settings */
// define( 'WPLANG', 'de_DE' );
// define( 'WP_LANG_DIR', dirname(__FILE__) . 'wordpress/languages' );

/* ADVANCED SETTINGS */

/* Compression */
// define( 'COMPRESS_CSS',        true );
// define( 'COMPRESS_SCRIPTS',    true );
// define( 'CONCATENATE_SCRIPTS', true );
// define( 'ENFORCE_GZIP',        true );

/* SSL */
// define( 'FORCE_SSL_LOGIN', true );
// define( 'FORCE_SSL_ADMIN', true );

/* CRON */
// define( 'DISABLE_WP_CRON', false );
// define( 'ALTERNATE_WP_CRON', false );

/* Cookie domain */
// define( 'COOKIE_DOMAIN', 'www.example.com' );

/* Max Memory Limit */
// define( 'WP_MAX_MEMORY_LIMIT', '256M' );

/* Cache */
// define( 'WP_CACHE', true );

/* Cleanup Image Edits */
// define( 'IMAGE_EDIT_OVERWRITE', true );

/* Override of default file permissions */
// define( 'FS_CHMOD_DIR', ( 0755 & ~ umask() ) );
// define( 'FS_CHMOD_FILE', ( 0644 & ~ umask() ) );

/* WordPress Upgrade Constants */

// Host running with a special installation setup involving symlinks. 
// You may need to define the path-related constants 
// (FTP_BASE, FTP_CONTENT_DIR, and FTP_PLUGIN_DIR). Often defining simply the base will be enough.

// Certain PHP installations shipped with a PHP FTP extension which is 
// incompatible with certain FTP servers. Under these rare situations, you may need to define FS_METHOD to “ftpsockets”.

// define( 'FS_METHOD', 'ftpext' );
// define( 'FTP_BASE', '/path/to/wordpress/' );
// define( 'FTP_CONTENT_DIR', '/path/to/wordpress/wp-content/' );
// define( 'FTP_PLUGIN_DIR ', '/path/to/wordpress/wp-content/plugins/' );
// define( 'FTP_PUBKEY', '/home/username/.ssh/id_rsa.pub' );
// define( 'FTP_PRIKEY', '/home/username/.ssh/id_rsa' );
// define( 'FTP_USER', 'username' );
// define( 'FTP_PASS', 'password' );
// define( 'FTP_HOST', 'ftp.example.org' );
// define( 'FTP_SSL', false );

/* Block External URL Requests */

// define( 'WP_HTTP_BLOCK_EXTERNAL', true );
// define( 'WP_ACCESSIBLE_HOSTS', 'api.wordpress.org,*.github.com' );
