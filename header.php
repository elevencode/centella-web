<?php

/**
 * The header for our theme
 *
 * This is the template that displays all of the <head> section and everything up until <div id="content">
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package centella
 */

?>
<!doctype html>
<html <?php language_attributes(); ?>>

<head>
	<?php require_once get_template_directory() . '/inc/google-scripts.php'; ?>
	<meta charset="<?php bloginfo('charset'); ?>">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="profile" href="https://gmpg.org/xfn/11">

	<?php wp_head(); ?>
</head>

<body <?php body_class(); ?>>
	<!-- Google Tag Manager (noscript) -->
	<noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-T37SKJP" height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
	<!-- End Google Tag Manager (noscript) -->
	<?php wp_body_open(); ?>
	<div id="page" class="site bootstrap-wrapper">
		<a class="skip-link screen-reader-text" href="#primary"><?php esc_html_e('Skip to content', 'centella'); ?></a>

		<header id="masthead" class="site-header">
			<div class="container">
				<div class="navigation-container">
					<div class="site-branding">
						<a href="<?php echo esc_url(home_url('/')); ?>" rel="home">
							<?php if (is_home() || is_page('about-us') || is_single()) : ?>
								<img src="<?php echo get_stylesheet_directory_uri() . '/assets/img/Centella_Logo_Dark.svg'; ?>" class="custom-logo" alt="Centella Logo Dark">
							<?php else : ?>
								<img src="<?php echo get_stylesheet_directory_uri() . '/assets/img/Centella_Logo.svg'; ?>" class="custom-logo" alt="Centella Logo">
							<?php endif; ?>
						</a>
					</div><!-- .site-branding -->

					<nav id="site-navigation" class="main-navigation<?php echo (!is_front_page() && is_home() || is_page('about-us') || is_single()) ? ' main-navigation--dark' : ''; ?>">
						<?php
						wp_nav_menu(
							array(
								'theme_location' => 'primary-menu',
								'menu_id'        => 'primary-menu',
							)
						);
						?>
					</nav><!-- #site-navigation -->
					<button id="mobile-nav-menu">
						<?php if (is_home() || is_page('about-us') || is_single()) : ?>
							<img src="<?php echo get_stylesheet_directory_uri() . '/assets/img/hamburger-menu-dark.svg' ?>" width="26" height="22" />
						<?php else : ?>
							<img src="<?php echo get_stylesheet_directory_uri() . '/assets/img/hamburger-menu.svg' ?>" width="26" height="22" />
						<?php endif; ?>
					</button>
				</div>
			</div>
		</header> <!-- masthead -->
		<?php if (is_front_page()) : ?>
			<div class="header-section">
				<div class="container">
					<div class="row header-row">
						<div class="col-md-6">
							<h1><?php the_field( 'header_title', 'option' ); ?></h1>
							<span style="height:10px;display:block"></span>
							<p>
								<?php the_field( 'header_subtitle', 'option' ); ?>
							</p>
							<p>
								<?php the_field( 'header_list', 'option' ); ?>
							</p>
							<span style="height:10px;display:block"></span>
							<a href="<?php echo get_permalink(19); ?>" class="button button-secondary">
								Get in touch
							</a>
						</div>
						<div class="col-md-6">
							<?php $header_image = get_field('header_image', 'option'); ?>
							<?php if ($header_image) : ?>
								<img class="header-image" src="<?php echo esc_url($header_image['url']); ?>" alt="<?php echo esc_attr($header_image['alt']); ?>" />
							<?php endif; ?>
						</div>
					</div>
				</div>
			</div>
			<div class="site-banner">
				<div class="container">
					<div class="row align-items-center">
						<?php if (have_rows('partner_banner_images', 'option')) : ?>
							<?php while (have_rows('partner_banner_images', 'option')) : the_row(); ?>
								<?php $banner_image_1 = get_sub_field('banner_image_1'); ?>
								<?php if ($banner_image_1) : ?>
									<div class="col-md-4 text-center">
										<img src="<?php echo esc_url($banner_image_1['url']); ?>" alt="<?php echo esc_attr($banner_image_1['alt']); ?>" />
									</div>
								<?php endif; ?>
								<?php $banner_image_2 = get_sub_field('banner_image_2'); ?>
								<?php if ($banner_image_2) : ?>
									<div class="col-md-4 text-center">
										<img src="<?php echo esc_url($banner_image_2['url']); ?>" alt="<?php echo esc_attr($banner_image_2['alt']); ?>" />
									</div>
								<?php endif; ?>
								<?php $banner_image_3 = get_sub_field('banner_image_3'); ?>
								<?php if ($banner_image_3) : ?>
									<div class="col-md-4 text-center">
										<img src="<?php echo esc_url($banner_image_3['url']); ?>" alt="<?php echo esc_attr($banner_image_3['alt']); ?>" />
									</div>
								<?php endif; ?>
								<?php $banner_image_4 = get_sub_field('banner_image_4'); ?>
								<!-- <?php if ($banner_image_4) : ?>
									<div class="col-md-3 text-center">
										<img src="<?php echo esc_url($banner_image_4['url']); ?>" alt="<?php echo esc_attr($banner_image_4['alt']); ?>" />
									</div>
								<?php endif; ?> -->
							<?php endwhile; ?>
						<?php endif; ?>
					</div>
				</div>
			</div>
		<?php elseif (is_home() || is_page('about-us') || is_single()) : ?>
			<div class="header-small header-small--light">

			</div>
		<?php elseif (is_page()) : ?>
			<div class="header-smaller">
				<div class="container">

				</div>
			</div>
		<?php else : ?>
			<div class="header-small">
				<div class="container">
					<div class="row">
						<div class="col-md-12">
							<h1><?php echo wp_title(); ?></h1>
						</div>
					</div>
				</div>
			</div>
		<?php endif; ?>