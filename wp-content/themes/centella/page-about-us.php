<?php

/**
 * The template for displaying all pages
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site may use a
 * different template.
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package centella
 */

get_header();
?>

<main id="primary" class="site-main">

	<section class="section section--bg-light">
		<div class="container">
			<div class="row">
				<div class="col-md-12">
					<?php the_field('about_us_section_1', 'option'); ?>
				</div>
			</div>
		</div>
	</section>
	<section class="section section--bg-white">
		<div class="container">
			<div class="row">
				<div class="col-md-11 offset-md-1">
					<?php the_field('about_us_section_2', 'option'); ?>
				</div>
			</div>
		</div>
	</section>
	<section class="section section--bg-light">
		<div class="container">
			<div class="row">
				<div class="col-md-12">
					<?php the_field('about_us_section_3', 'option'); ?>
				</div>
			</div>
		</div>
	</section>

</main><!-- #main -->

<?php
get_footer();
