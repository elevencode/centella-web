<?php

/**
 * The template for displaying all pages
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site may use a
 * different template.
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package centella
 */

get_header();
?>
<main id="primary" class="site-main">
	<section class="section section--bg-dark">
		<div class="container">
			<div class="row">
				<div class="col-md-12">
					<h1 class="thank-you-title">Thank you for contacting us!</h1>
				</div>
			</div>
		</div>
	</section>

</main><!-- #main -->
<?php
get_footer();
