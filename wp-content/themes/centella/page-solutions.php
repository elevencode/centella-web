<?php

/**
 * The template for displaying all pages
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site may use a
 * different template.
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package centella
 */

get_header();
?>

<main id="primary" class="site-main">
	<section class="section section--bg-white">
		<div class="container">
			<div class="row">
				<?php if (have_rows('solution_section_1', 'option')) : ?>
					<?php while (have_rows('solution_section_1', 'option')) : the_row(); ?>
						<div class="col-md-6"><?php the_sub_field('solution_section_wyswyg'); ?></div>
						<?php $solution_section_image = get_sub_field('solution_section_image'); ?>
						<?php if ($solution_section_image) : ?>
							<div class="col-md-6"><img src="<?php echo esc_url($solution_section_image['url']); ?>" alt="<?php echo esc_attr($solution_section_image['alt']); ?>" /></div>
						<?php endif; ?>
					<?php endwhile; ?>
				<?php endif; ?>
			</div>
		</div>
	</section>

	<section class="section section--bg-light">
		<div class="container">
			<div class="row">
				<div class="col-md-12">
					<span class="section-title-font"><?php the_field('solution_section_2', 'option', false, false); ?></span>
				</div>
			</div>
		</div>
	</section>

	<section class="section section--bg-white">
		<div class="container">
			<div class="row">
				<?php if (have_rows('solution_section_3', 'option')) : ?>
					<?php while (have_rows('solution_section_3', 'option')) : the_row(); ?>
						<div class="col-md-6"><?php the_sub_field('solution_section_wyswyg'); ?></div>
						<?php $solution_section_image = get_sub_field('solution_section_image'); ?>
						<?php if ($solution_section_image) : ?>
							<div class="col-md-6"><img src="<?php echo esc_url($solution_section_image['url']); ?>" alt="<?php echo esc_attr($solution_section_image['alt']); ?>" /></div>
						<?php endif; ?>
					<?php endwhile; ?>
				<?php endif; ?>
			</div>
		</div>
	</section>

	<section class="section section--bg-light">
		<div class="container">
			<div class="row">
				<div class="col-md-12">
					<span class="section-title-font section-title-font--large"><?php the_field('solution_section_4', 'option'); ?></span>
				</div>
			</div>
		</div>
	</section>

	<section class="section section--bg-white">
		<div class="container">
			<div class="row">
				<?php if (have_rows('solution_section_5', 'option')) : ?>
					<?php while (have_rows('solution_section_5', 'option')) : the_row(); ?>
						<div class="col-md-6"><?php the_sub_field('solution_section_wyswyg'); ?></div>
						<?php $solution_section_image = get_sub_field('solution_section_image'); ?>
						<?php if ($solution_section_image) : ?>
							<div class="col-md-6"><img src="<?php echo esc_url($solution_section_image['url']); ?>" alt="<?php echo esc_attr($solution_section_image['alt']); ?>" /></div>
						<?php endif; ?>
					<?php endwhile; ?>
				<?php endif; ?>
			</div>
		</div>
	</section>

	<section class="section section--bg-light">
		<div class="container">
			<div class="row">
				<div class="col-md-12">
					<span class="section-title-font"><?php the_field('solution_section_6', 'option', false, false); ?></span>
				</div>
			</div>
		</div>
	</section>

	<section class="section section--bg-dark">
		<div class="container">
			<div class="row">
				<div class="col-md-12" style="text-align: center;">
					<h2><?php esc_html_e("Our Process", 'centella'); ?></h2>
				</div>
				<?php if (have_rows('solution_section_7', 'option')) : ?>
					<?php while (have_rows('solution_section_7', 'option')) : the_row(); ?>
						<?php $solution_section_image_left = get_sub_field('solution_section_image_left'); ?>
						<?php if ($solution_section_image_left) : ?>
							<div class="col-md-6"><img src="<?php echo esc_url($solution_section_image_left['url']); ?>" alt="<?php echo esc_attr($solution_section_image_left['alt']); ?>" /></div>
						<?php endif; ?>
						<?php $solution_section_image_right = get_sub_field('solution_section_image_right'); ?>
						<?php if ($solution_section_image_right) : ?>
							<div class="col-md-6"><img src="<?php echo esc_url($solution_section_image_right['url']); ?>" alt="<?php echo esc_attr($solution_section_image_right['alt']); ?>" /></div>
						<?php endif; ?>
					<?php endwhile; ?>
				<?php endif; ?>
			</div>
		</div>
	</section>

</main><!-- #main -->

<?php
get_footer();
