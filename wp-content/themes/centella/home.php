<?php

/**
 * The template for displaying all pages
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site may use a
 * different template.
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package centella
 */

get_header();
?>

<main id="primary" class="site-main">
	<div class="">
		<div class="container">
			<div class="row">

				<?php
				// the query
				$the_query = new WP_Query(array(
					'category_name' => 'featured',
					'posts_per_page' => 1,
				));
				?>

				<?php if ($the_query->have_posts()) : ?>
					<?php while ($the_query->have_posts()) : $the_query->the_post(); ?>
						<div class="col-md-8">
							<?php get_template_part('template-parts/content', 'blog-post'); ?>
						</div>
					<?php endwhile; ?>
					<?php wp_reset_postdata(); ?>
				<?php endif; ?>
				<div class="col-md-4 blog-archive-sidebar">
					<div class="blog-archive-category-picker">
						<select class="category-select">
							<option value="-1">Category</option>
							<?php
							$args_cat = [
								'orderby' => 'name',
								'order' => 'ASC',
								'hide_empty' => 0,
							];

							$categories = get_categories($args_cat);
							if (!empty($categories)) :
								foreach ($categories as $category) : ?>
									<option value="<?php echo $category->term_id; ?>">
										<?php echo $category->name; ?>
									</option>
							<?php endforeach;
							endif;
							?>
						</select>
					</div>
					<div class="blog-archive-subs">
						<div class="blog-archive-subs-header">
							<h3>Newsletter</h3>
							<p>Subscribe to our newsletter</p>
						</div>
						<div class="form-group" style="margin-top:auto;">
							<input class="input-field input-field--fw" type="text" placeholder="email" />
							<button class="btn btn--primary">Subscribe</button>
						</div>

					</div>
				</div>
			</div>
			<div class="row posts-area">

			</div>
		</div>

	</div>
</main><!-- #main -->
<script type="text/javascript">
	(function($) {
		$(document).ready(function() {
			$.ajax({
				type: "post",
				url: centella_globals.ajax_url,
				data: {
					action: "centella_change_category",
					catTermId: "-1",
					_ajax_nonce: centella_globals.nonce,
				},
				success: function(response) {
					$(".posts-area").html(response.replace(/\"/g, ""));
				},
			});
		});
	})(jQuery);
</script>

<?php
get_footer();
