<?php

/**
 * centella functions and definitions
 *
 * @link https://developer.wordpress.org/themes/basics/theme-functions/
 *
 * @package centella
 */

if (!defined('_S_VERSION')) {
	// Replace the version number of the theme on each release.
	define('_S_VERSION', '1.0.0');
}

if (!function_exists('centella_setup')) :
	/**
	 * Sets up theme defaults and registers support for various WordPress features.
	 *
	 * Note that this function is hooked into the after_setup_theme hook, which
	 * runs before the init hook. The init hook is too late for some features, such
	 * as indicating support for post thumbnails.
	 */
	function centella_setup()
	{
		/*
		 * Make theme available for translation.
		 * Translations can be filed in the /languages/ directory.
		 * If you're building a theme based on centella, use a find and replace
		 * to change 'centella' to the name of your theme in all the template files.
		 */
		load_theme_textdomain('centella', get_template_directory() . '/languages');

		// Add default posts and comments RSS feed links to head.
		add_theme_support('automatic-feed-links');

		/*
		 * Let WordPress manage the document title.
		 * By adding theme support, we declare that this theme does not use a
		 * hard-coded <title> tag in the document head, and expect WordPress to
		 * provide it for us.
		 */
		add_theme_support('title-tag');

		/*
		 * Enable support for Post Thumbnails on posts and pages.
		 *
		 * @link https://developer.wordpress.org/themes/functionality/featured-images-post-thumbnails/
		 */
		add_theme_support('post-thumbnails');

		// This theme uses wp_nav_menu() in one location.
		register_nav_menus(
			array(
				'primary-menu' => esc_html__('Primary', 'centella'),
				'footer-menu' => esc_html__('Footer Menu', 'centella')
			)
		);

		/*
		 * Switch default core markup for search form, comment form, and comments
		 * to output valid HTML5.
		 */
		add_theme_support(
			'html5',
			array(
				'search-form',
				'comment-form',
				'comment-list',
				'gallery',
				'caption',
				'style',
				'script',
			)
		);

		// Set up the WordPress core custom background feature.
		add_theme_support(
			'custom-background',
			apply_filters(
				'centella_custom_background_args',
				array(
					'default-color' => 'ffffff',
					'default-image' => '',
				)
			)
		);

		// Add theme support for selective refresh for widgets.
		add_theme_support('customize-selective-refresh-widgets');

		/**
		 * Add support for core custom logo.
		 *
		 * @link https://codex.wordpress.org/Theme_Logo
		 */
		add_theme_support(
			'custom-logo',
			array(
				'height'      => 250,
				'width'       => 250,
				'flex-width'  => true,
				'flex-height' => true,
			)
		);
	}
endif;
add_action('after_setup_theme', 'centella_setup');

/**
 * Set the content width in pixels, based on the theme's design and stylesheet.
 *
 * Priority 0 to make it available to lower priority callbacks.
 *
 * @global int $content_width
 */
function centella_content_width()
{
	$GLOBALS['content_width'] = apply_filters('centella_content_width', 640);
}
add_action('after_setup_theme', 'centella_content_width', 0);

/**
 * Register widget area.
 *
 * @link https://developer.wordpress.org/themes/functionality/sidebars/#registering-a-sidebar
 */
function centella_widgets_init()
{
	register_sidebar(
		array(
			'name'          => esc_html__('Sidebar', 'centella'),
			'id'            => 'sidebar-1',
			'description'   => esc_html__('Add widgets here.', 'centella'),
			'before_widget' => '<section id="%1$s" class="widget %2$s">',
			'after_widget'  => '</section>',
			'before_title'  => '<h2 class="widget-title">',
			'after_title'   => '</h2>',
		)
	);
}
add_action('widgets_init', 'centella_widgets_init');

/**
 * Enqueue scripts and styles.
 */
function centella_scripts()
{
	wp_enqueue_style('centella-main-style', get_template_directory_uri() . '/assets/css/style.css', array(), _S_VERSION);
	wp_enqueue_style('centella-style', get_stylesheet_uri(), array(), _S_VERSION);
	wp_style_add_data('centella-style', 'rtl', 'replace');

	wp_enqueue_script('centella-navigation', get_template_directory_uri() . '/js/slick.js', array(), _S_VERSION, true);

	if (is_singular() && comments_open() && get_option('thread_comments')) {
		wp_enqueue_script('comment-reply');
	}
}
add_action('wp_enqueue_scripts', 'centella_scripts');

/**
 * Implement the Custom Header feature.
 */
require get_template_directory() . '/inc/custom-header.php';

/**
 * Custom template tags for this theme.
 */
require get_template_directory() . '/inc/template-tags.php';

/**
 * Functions which enhance the theme by hooking into WordPress.
 */
require get_template_directory() . '/inc/template-functions.php';

/**
 * Customizer additions.
 */
require get_template_directory() . '/inc/customizer.php';

/**
 * Load Jetpack compatibility file.
 */
if (defined('JETPACK__VERSION')) {
	require get_template_directory() . '/inc/jetpack.php';
}

/**
 *	Disable comments globally
 */
require get_template_directory() . '/inc/disable-comments.php';

/**
 * Enable SVG
 */
require get_template_directory() . '/inc/enable-svg.php';

function centella_frontend_scripts()
{

	wp_enqueue_script(
		'centella-frontend',
		get_stylesheet_directory_uri() . '/js/category-select.js',
		['jquery'],
		time(),
		true
	);

	wp_localize_script(
		'centella-frontend',
		'centella_globals',
		[
			'ajax_url'    => admin_url('admin-ajax.php'),
			'nonce'       => wp_create_nonce('centella_category_nonce'),
		]
	);
}
add_action('wp_enqueue_scripts', 'centella_frontend_scripts');

function centella_change_category()
{
	// Change the parameter of check_ajax_referer() to 'centella_change_category_nonce'
	check_ajax_referer('centella_category_nonce', 'security');

	$args = array('exclude' => 1, 'fields' => 'ids');
	$exclude_uncategorized = get_terms('category', $args);

	if ($_POST['catTermId'] == "-1") {
		$args = [
			'posts_per_page' => -1,
			'order' => 'ASC',
			'orderby' => 'title',
			'category__in' => $exclude_uncategorized
		];
	} else {
		$args = [
			'posts_per_page' => -1,
			'order' => 'ASC',
			'orderby' => 'title',
			'cat' =>  $_POST['catTermId']
		];
	}
	
	$response = '';

	$mainQuery = new WP_Query($args);
	
	while ($mainQuery->have_posts()):
		$mainQuery->the_post();
		$response .= get_template_part('template-parts/content', 'blog-post');
	endwhile; // End of the loop.
	
	wp_reset_postdata(); // reset the query 
	
	$response = json_encode($response);
	echo $response;
	die();
}
add_action('wp_ajax_centella_change_category', 'centella_change_category');
add_action('wp_ajax_nopriv_centella_change_category', 'centella_change_category');
