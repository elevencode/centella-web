<?php

/**
 * Template part for displaying posts
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package centella
 */

?>

<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
	<div class="container">
		<div class="row">
			<div class="col-md-12">
				<header class="entry-header">
					<?php
					if (is_singular()) :
						the_title('<h1 class="blog-single-title">', '</h1>');
					else :
						the_title('<h2 class="blog-single-title"><a href="' . esc_url(get_permalink()) . '" rel="bookmark">', '</a></h2>');
					endif;

					if ('post' === get_post_type()) :
					?>
						<div class="blog-item-footer">
							<div class="blog-item-reading-time"><?php the_field('reading_time'); ?> min</div>
							<div class="blog-item-divider"></div>
							<?php
							$category = get_the_category();
							$firstCategory = $category[0]->cat_name;
							?>

							<div class="blog-item-category"><?php echo $firstCategory ?></div>
						</div>
						<div class="entry-meta blog-single-meta">
							<?php
							centella_posted_by();
							centella_posted_on();
							?>
						</div><!-- .entry-meta -->
					<?php endif; ?>
				</header><!-- .entry-header -->
			</div>
			<div class="col-md-12">
				<?php centella_post_thumbnail(); ?>
			</div>
			<div class="col-md-8">
				<div class="entry-content">
					<?php
					the_content(
						sprintf(
							wp_kses(
								/* translators: %s: Name of current post. Only visible to screen readers */
								__('Continue reading<span class="screen-reader-text"> "%s"</span>', 'centella'),
								array(
									'span' => array(
										'class' => array(),
									),
								)
							),
							wp_kses_post(get_the_title())
						)
					);

					wp_link_pages(
						array(
							'before' => '<div class="page-links">' . esc_html__('Pages:', 'centella'),
							'after'  => '</div>',
						)
					);
					?>
				</div><!-- .entry-content -->
			</div>
			<div class="col-md-4 blog-archive-sidebar">
				<div class="blog-archive-subs blog-archive-subs--max-height">
					<div class="blog-archive-subs-header">
						<h3>Newsletter</h3>
						<p>Subscribe to our newsletter</p>
					</div>
					<div class="form-group" style="margin-top: auto;">
						<input class="input-field input-field--fw" type="text" placeholder="email" />
						<button class="btn btn--primary">Subscribe</button>
					</div>
				</div>
			</div>
		</div>
	</div>
</article><!-- #post-<?php the_ID(); ?> -->