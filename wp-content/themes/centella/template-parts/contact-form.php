<section class="section section--bg-light">

    <div class="container">

        <div class="row">

            <div class="col-md-6">

                <?php the_field('contact_form_wyswyg', 'option'); ?>

            </div>

            <div class="col-md-6">

                <?php echo do_shortcode("[wpforms id='148' title='false']"); ?>

            </div>

        </div>

    </div>

</section>