<?php



/**

 * Template part for displaying page content in page.php

 *

 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/

 *

 * @package centella

 */



?>
<div class="col-md-4">
    <article id="post-<?php the_ID(); ?>" class="blog-item">

        <a class="blog-item-permalink" href="<?php the_permalink(); ?>">

            <div class="blog-image">

                <?php the_post_thumbnail(); ?>

            </div>

            <div class="blog-content">

                <?php the_title('<h2 class="blog-title">', '</h2>'); ?>

                <?php

                wp_link_pages(

                    array(

                        'before' => '<div class="page-links">' . esc_html__('Pages:', 'centella'),

                        'after'  => '</div>',

                    )

                );

                ?>

                <div class="blog-item-footer">

                    <div class="blog-item-reading-time"><?php the_field('reading_time'); ?> min</div>

                    <div class="blog-item-divider"></div>

                    <?php

                    $category = get_the_category();

                    $firstCategory = $category[0]->cat_name;

                    ?>



                    <div class="blog-item-category"><?php echo $firstCategory ?></div>

                </div>

            </div><!-- .entry-content -->

        </a>

    </article><!-- #post-<?php the_ID(); ?> -->
</div>