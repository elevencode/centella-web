  <?php
	/**
	 * The template for displaying the footer
	 *
	 * Contains the closing of the #content div and all content after.
	 *
	 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
	 *
	 * @package centella
	 */

	?>

  <footer id="colophon" class="site-footer<?php echo (is_page('thank-you')) ? ' site-footer--dark' : ''; ?> ">
  	<div class="site-info">
  		<div class="container">
  			<div class="row">
  				<div class="col-md-12">
  					<?php if (have_rows('footer_social_icons', 'option')) : ?>
  						<?php while (have_rows('footer_social_icons', 'option')) : the_row(); ?>
  							<a class="footer-social footer-social--facebook" href="<?php the_sub_field('facebook'); ?>" target="_blank"></a>
  							<a class="footer-social footer-social--instagram" href="<?php the_sub_field('instagram'); ?>" target="_blank"></a>
  							<a class="footer-social footer-social--linkedin" href="<?php the_sub_field('linkedin'); ?>" target="_blank"></a>
  						<?php endwhile; ?>
  					<?php endif; ?>
  				</div>
  				<div class="col-md-12">
  					<?php
						wp_nav_menu(
							array(
								'theme_location' => 'footer-menu',
								'menu_id'        => 'footer-menu',
								'menu_class'	 => 'footer-menu'
							)
						);
						?>
  				</div>
  				<div class="col-md-12">
  					<small style="font-style:italic"><?php echo esc_html_e('Centella Media', 'centella') . ' ' . date("Y"); ?></small>
  				</div>
  			</div>
  		</div>
  	</div><!-- .site-info -->
  </footer><!-- #colophon -->
  </div><!-- #page -->

  <?php wp_footer(); ?>
  <?php if (is_front_page()) : ?>
  	<script type="text/javascript">
  		(function($) {
  			$('.slick-slider').slick({
				arrows: false,
  				dots: false,
  				infinite: false,
  				speed: 300,
  				slidesToShow: 4,
  				slidesToScroll: 4,
  				responsive: [{
  						breakpoint: 1024,
  						settings: {
  							slidesToShow: 3,
  							slidesToScroll: 3,
  							infinite: true,
  						}
  					},
  					{
  						breakpoint: 600,
  						settings: {
  							slidesToShow: 2,
  							slidesToScroll: 2
  						}
  					},
  					{
  						breakpoint: 480,
  						settings: {
  							slidesToShow: 1,
  							slidesToScroll: 1
  						}
  					}
  					// You can unslick at a given breakpoint now by adding:
  					// settings: "unslick"
  					// instead of a settings object
  				]
  			});
  			// use $ here safely
  		})(jQuery);
  	</script>
  <?php endif; ?>
  </body>

  </html>