(function ($) {
	$(".category-select").change(function () {
		console.log("enters here");
		let catTermId = $(".category-select").val()

		$.ajax({
			type: "post",
			url: centella_globals.ajax_url,
			data: {
				action: "centella_change_category",
				catTermId: catTermId,
				_ajax_nonce: centella_globals.nonce,
			},
			success: function (response) {
				console.log(response);
				$(".posts-area").html(response.replace(/\"/g, ""));
			},
		});
	});
})(jQuery);
