<?php

/**
 * The template for displaying all pages
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site may use a
 * different template.
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package centella
 */

get_header();
?>

<main id="primary" class="site-main">
	<section class="section section--bg-white">
		<div class="container">
			<div class="row row-inverse-mobile">
				<?php if (have_rows('homapege_section_1', 'option')) : ?>
					<?php while (have_rows('homapege_section_1', 'option')) : the_row(); ?>
						<div class="col-md-6">
							<?php the_sub_field('homepage_section_1_wyswyg'); ?>
						</div>
						<div class="col-md-6">
							<?php $homapge_section_1_image = get_sub_field('homapge_section_1_image'); ?>
							<?php if ($homapge_section_1_image) : ?>
								<img src="<?php echo esc_url($homapge_section_1_image['url']); ?>" alt="<?php echo esc_attr($homapge_section_1_image['alt']); ?>" />
							<?php endif; ?>
						</div>
					<?php endwhile; ?>
				<?php endif; ?>
			</div>
		</div>
	</section>

	<section class="section section--bg-light">
		<div class="container">
			<div class="row">
				<?php if (have_rows('homapege_section_2', 'option')) : ?>
					<?php while (have_rows('homapege_section_2', 'option')) : the_row(); ?>
						<div class="col-md-6">
							<?php $homapge_section_1_image = get_sub_field('homapge_section_image'); ?>
							<?php if ($homapge_section_1_image) : ?>
								<img src="<?php echo esc_url($homapge_section_1_image['url']); ?>" alt="<?php echo esc_attr($homapge_section_1_image['alt']); ?>" />
							<?php endif; ?>
						</div>
						<div class="col-md-6">
							<?php the_sub_field('homepage_section_wyswyg'); ?>
						</div>
					<?php endwhile; ?>
				<?php endif; ?>
			</div>
		</div>
	</section>

	<section class="section section--bg-white">
		<div class="container">
			<div class="row row-inverse-mobile">
				<?php if (have_rows('homapege_section_3', 'option')) : ?>
					<?php while (have_rows('homapege_section_3', 'option')) : the_row(); ?>
						<div class="col-md-6">
							<?php the_sub_field('homepage_section_wyswyg'); ?>
						</div>
						<div class="col-md-6">
							<?php $homapge_section_1_image = get_sub_field('homapge_section_image'); ?>
							<?php if ($homapge_section_1_image) : ?>
								<img src="<?php echo esc_url($homapge_section_1_image['url']); ?>" alt="<?php echo esc_attr($homapge_section_1_image['alt']); ?>" />
							<?php endif; ?>
						</div>

					<?php endwhile; ?>
				<?php endif; ?>
			</div>
		</div>
	</section>

	<section class="section section--bg-dark">
		<div class="container">
			<div class="row">
				<?php if (have_rows('homapege_section_5', 'option')) : ?>
					<?php while (have_rows('homapege_section_5', 'option')) : the_row(); ?>
						<div class="col-md-6 div-number div-number--one"><?php the_sub_field('homepage_section_wyswyg_left'); ?></div>
						<div class="col-md-6 div-number div-number--two"><?php the_sub_field('homepage_section_wyswyg_right'); ?></div>
					<?php endwhile; ?>
				<?php endif; ?>
			</div>
		</div>
	</section>
	<?php $homepage_section_background_image = get_field('homepage_section_background_image', 'option'); ?>
	<?php if ($homepage_section_background_image) : ?>
		<section class="section section--bg-full-image section--bg-dark " style="background-image:url('<?php echo esc_url($homepage_section_background_image['url']); ?>');">
			<div class="container">
				<div class="row">
					<div class="col-md-12" style="text-align:center;">
						<h2>Customer retention 2+ years</h2>
						<a href="<?php echo get_permalink(19); ?>" class="button button-secondary">
							Get in touch
						</a>
					</div>
				</div>
			</div>
		</section>
	<?php endif; ?>

	<?php get_template_part('template-parts/contact-form'); ?>

</main><!-- #main -->

<?php
get_footer();
